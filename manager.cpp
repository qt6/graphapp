#include "manager.h"

#include <QLineSeries>
#include <QRandomGenerator64>




Shower::Shower()
{

}


Generator::Generator()
{

    m_timerId = 0;
    m_timerRate = 0;
    m_step = 0;

}

void Generator::readFromBuf(QPolygonF &values, int valSize)
{
    QMutexLocker lc(&m_mtx);


    int readSize = qMin(m_buf.size(), valSize);

    m_buf.mid(0, readSize);
    m_buf.remove(0, readSize);


}


void Generator::startGenerate()
{
    changeTimeRate(1000);
}

void Generator::changeTimeRate(int rate)
{
    if(m_timerId){
        killTimer(m_timerId);
        m_timerId = 0;
    }

    m_timerId = startTimer(m_timerRate = rate);
}

void Generator::timerEvent(QTimerEvent *e)
{
    QMutexLocker lc(&m_mtx);

    for(int i = 0; i < 4 * m_timerRate / 1000; ++i){
        auto val = QRandomGenerator64::global()->bounded(10000, 20000);
        m_buf.append(QPointF(i / (4.0 * m_timerRate / 1000) + m_step, val));
    }

    m_step += (m_timerRate / 1000.0);
}


Manager::Manager(QObject *parent)
    : QObject{parent}
{

    m_generator = new Generator;
    m_shower = new Shower;

    m_generator->moveToThread(&m_generatorThread);
    m_shower->moveToThread(&m_showerThread);

    connect(&m_generatorThread, &QThread::finished, m_generator, &Generator::deleteLater);
    connect(&m_generatorThread, &QThread::started, m_generator, &Generator::startGenerate);

    connect(&m_generatorThread, &QThread::started, &m_showerThread, &QThread::start);


    m_generatorThread.start();

}
