#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QChartView>
#include <QChart>
#include <QLineSeries>

#include "manager.h"

using namespace QtCharts;

class Widget : public QChartView
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();



private:

    // virtual void timerEvent(QTimerEvent *e) override;
    virtual void wheelEvent(QWheelEvent *e) override;
    virtual void keyPressEvent(QKeyEvent *e) override;
    virtual void keyReleaseEvent(QKeyEvent *e) override;
    virtual void mousePressEvent(QMouseEvent *e) override;
    virtual void mouseReleaseEvent(QMouseEvent *e) override;
    virtual void mouseMoveEvent(QMouseEvent *e) override;

    QChart *m_chart;
    QLineSeries *m_series;


    Manager m_manager;

    // int m_timerId;
    // float m_timerRate;

    float step;

    QVector<float>buf;


    bool isControl;
    bool isShift;
    bool isMouse;

    QPointF clickPos;

    float vertCenter;
    float horCenter;

    float vertScale;
    float horScale;

    float horOffset;
    float vertOffset;
};
#endif // WIDGET_H
