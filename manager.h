#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QThread>
#include <QPolygonF>
#include <QMutex>

namespace QtCharts {
class QLineSeries;
};

using namespace QtCharts;

class Shower : public QObject
{
    Q_OBJECT

public:
    explicit Shower();


    Q_INVOKABLE void setSeries(QLineSeries *series);


private:

    QLineSeries *m_series;

};

class Generator : public QObject
{
    Q_OBJECT
public:
    explicit Generator();


    void readFromBuf(QPolygonF &values, int valSize);


signals:

public slots:

    void startGenerate();


    void changeTimeRate(int rate);
private:

    virtual void timerEvent(QTimerEvent *e) override;


    QMutex m_mtx;
    QPolygonF m_buf;

    int m_timerId;
    int m_timerRate;
    float m_step;

};



class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(QObject *parent = nullptr);
    ~Manager()
    {
        m_generatorThread.quit();
        m_generatorThread.wait();
    }

    void setSeries(QLineSeries *series);


signals:

private:

    // virtual void timerEvent(QTimerEvent *e) override;

    QThread m_generatorThread;
    Generator *m_generator;

    QThread m_showerThread;
    Shower *m_shower;

    // QThread m_thread;
    // Driver *m_driver;



    int m_timerId;
    int m_timerRate;

};

#endif // MANAGER_H
