#include "widget.h"

#include <QDebug>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <cmath>
#include <QRandomGenerator64>

Widget::Widget(QWidget *parent)
    : QChartView(parent)
{

    m_series = new QLineSeries(this);


    m_manager.setSeries(m_series);

    chart()->legend()->hide();
    chart()->addSeries(m_series);
    chart()->createDefaultAxes();


    chart()->axes().last()->setRange(10000, 20000);


    // m_timerId = startTimer(m_timerRate = 1000);

    step = 0;

    isControl = false;
    isMouse = false;
    isShift = false;

    vertScale = 1;
    horScale = 1;

    horOffset = 0;
    vertOffset = 0;
    vertCenter = 14000;

}

Widget::~Widget()
{

}

// void Widget::timerEvent(QTimerEvent *e)
// {


//     m_series->clear();

//     for(int i = 0; i < 4 * m_timerRate; ++i){

//         m_series->append(i / (4.0f * m_timerRate) + step, QRandomGenerator64::global()->bounded(10000, 20000));
//     }

//     // chart()->axes().first()->setRange(step, step * horScale + 1);


//     step += (m_timerRate / 1000.0f);
// }

void Widget::wheelEvent(QWheelEvent *e)
{
    if(isControl)
        horScale *= pow(1.1, e->angleDelta().y() / -120.0);
    else
        vertScale *= pow(1.1, e->angleDelta().y() / -120.0);


    chart()->axes().first()->setRange(horCenter - horScale + horOffset, horCenter + horScale + horOffset);
    chart()->axes().last()->setRange(vertCenter - vertScale + vertOffset, vertCenter + vertScale + vertOffset);

}

void Widget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Control)
        isControl = true;
    else if(e->key() == Qt::Key_Shift)
        isShift = true;
}

void Widget::keyReleaseEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Control)
        isControl = false;
    else if(e->key() == Qt::Key_Shift)
        isShift = false;

}

void Widget::mousePressEvent(QMouseEvent *e)
{
    // qInfo()<<e->pos();


    QChartView::mousePressEvent(e);

    if(e->button() == Qt::LeftButton){
        isMouse = true;

        clickPos = e->pos();
    }


}

void Widget::mouseReleaseEvent(QMouseEvent *e)
{


    QChartView::mouseReleaseEvent(e);

    if(e->button() == Qt::LeftButton)
        isMouse = false;
}

void Widget::mouseMoveEvent(QMouseEvent *e)
{
    QChartView::mouseMoveEvent(e);

    if(isMouse){

        // vertOffset = (e->pos().y() - clickPos.y()) / 100.0;
        horOffset = (clickPos.x() - e->pos().x()) / 1000.0;

        chart()->axes().first()->setRange(horCenter - horScale + horOffset, horCenter + horScale + horOffset);
        chart()->axes().last()->setRange(vertCenter - vertScale + vertOffset, vertCenter + vertScale + vertOffset);

        horCenter += horOffset;
        // vertCenter += vertOffset;
    }

}
